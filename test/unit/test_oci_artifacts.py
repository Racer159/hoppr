"""
Test module for oci_artifacts class
"""

# pylint: disable=protected-access

import test

from pathlib import Path
from typing import List

import pytest

from oras.client import OrasClient  # type: ignore[import]
from pytest import MonkeyPatch

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr import oci_artifacts
from hoppr.exceptions import HopprLoadDataError
from hoppr.models.credentials import CredentialRequiredService, Credentials

sbom_path = Path(test.__file__).parent / "resources" / "bom" / "unit_bom3_mini.json"


def test_pull_artifact(find_credentials_fixture: CredentialRequiredService, monkeypatch: MonkeyPatch):
    """
    Test oci_artifacts.pull_artifact function success
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=OrasClient, name="pull", value=lambda *args, **kargs: [str(sbom_path)])

    result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image:1.2.3")
    assert result == hoppr.utils.load_file(sbom_path)


def test_pull_artifact_to_disk(monkeypatch: MonkeyPatch):
    """
    Test oci_artifacts.pull_artifact_to_disk function success
    """
    monkeypatch.setattr(target=Credentials, name="find", value=lambda *args, **kargs: None)
    monkeypatch.setattr(target=OrasClient, name="pull", value=lambda *args, **kargs: [str(sbom_path)])

    result = oci_artifacts.pull_artifact_to_disk("registry.test.com/my/repo/image:1.2.3", "/fake/out/dir")

    assert result == sbom_path


def test_pull_artifact_discover_version_preserves_latest(monkeypatch: MonkeyPatch):
    """
    Test oci_artifacts.pull_artifact function success while preserving latest version tag
    """

    monkeypatch.setattr(target=Credentials, name="find", value=lambda *args, **kargs: None)
    monkeypatch.setattr(target=OrasClient, name="pull", value=lambda *args, **kargs: [str(sbom_path)])

    result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image:latest")
    assert result == hoppr.utils.load_file(sbom_path)


def test_pull_artifact_discover_version_simple_semver(
    find_credentials_fixture: CredentialRequiredService, monkeypatch: MonkeyPatch
):
    """
    Test oci_artifacts.pull_artifact function success with allow version discovery
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(
        target=OrasClient,
        name="get_tags",
        value=lambda *args, **kargs: dict(tags=["main", "dev", "feature-branch", "0.1.1", "latest", "0.0.1", "0.0.2"]),
    )
    monkeypatch.setattr(target=OrasClient, name="pull", value=lambda *args, **kargs: [str(sbom_path)])

    result = oci_artifacts.pull_artifact("registry.test.com/my/repo/image", allow_version_discovery=True)
    assert result == hoppr.utils.load_file(sbom_path)


def test_pull_without_discovery_raises_error():
    """
    Test oci_artifacts.pull_artifact function raises error when version discovery is false
    """
    with pytest.raises(HopprLoadDataError):
        oci_artifacts.pull_artifact("registry.test.com/my/repo/image", allow_version_discovery=False)


def test_pull_artifact_load_file_fail(monkeypatch: MonkeyPatch):
    """
    Test oci_artifacts.pull_artifact function failure
    """

    monkeypatch.setattr(target=oci_artifacts.utils, name="load_file", value=lambda *args, **kargs: [])
    monkeypatch.setattr(target=oci_artifacts, name="_pull_artifact", value=lambda *args, **kargs: Path("artifact"))

    with pytest.raises(HopprLoadDataError):
        oci_artifacts.pull_artifact("registry.test.com/my/repo/image")


SEMVER_START_WITH_V = r"^v(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$"
# Explicitly not part of TestCase in order to allow use of parametrize
@pytest.mark.parametrize(
    ["version_list", "regex"],
    [
        (["1.2.3", "1.2.3-rc.2", "1.0.0"], oci_artifacts.SEM_VER_FULL_REGEX),
        (["1.2.3", "1.2.3-rc.2", "2.0.0"], oci_artifacts.SEM_VER_FULL_REGEX),
        (["1.2.3", "1.2.3-rc.2", "1.0.0"], oci_artifacts.SEM_VER_CORE_REGEX),
        (["v1.2.3", "v1.2.3-rc.2", "v1.0.0"], SEMVER_START_WITH_V),
    ],
)
def test_discover_version_behavior(monkeypatch: MonkeyPatch, version_list: List[str], regex: str):
    """Test the exact behavior of discover version with various tag patterns and different regexs"""
    with monkeypatch.context() as patch:
        patch.setattr(target=OrasClient, name="get_tags", value=lambda *args, **kwargs: dict(tags=version_list))
        oci_artifacts._discover_highest_version_tag("reg/my/artifact", version_regex=regex)
