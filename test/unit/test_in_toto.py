"""
Test module for the in_toto module
"""

# pylint: disable=missing-function-docstring
# pylint: disable=protected-access

import test

from pathlib import Path

from in_toto.models.metadata import Metablock  # type: ignore
from pytest import MonkeyPatch
from securesystemslib.interface import generate_and_write_rsa_keypair  # type: ignore

from hoppr.in_toto import HopprInTotoLinks, _get_products, generate_in_toto_layout
from hoppr.models.transfer import Transfer

transfer_file = Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml"
TRANSFER = Transfer.load(transfer_file)


def test__get_products():
    stages_expected = ["_collect_metadata", "_delta_sbom_", "Collect", "Bundle", "_finalize"]
    products_expected = {
        "_delta_sbom_": [
            "generic/_metadata_/_previous_bom.json",
            "generic/_metadata_/_delivered_bom.json",
            "generic/_metadata_/_intermediate__delta_sbom__delivered_bom.json",
        ],
        "_collect_metadata": ["generic/_metadata_/*"],
        "Collect": [
            "deb/*",
            "docker/*",
            "git/*",
            "gitlab/*",
            "github/*",
            "helm/*",
            "maven/*",
            "pypi/*",
            "rpm/*",
            "binary/*",
            "generic/*",
            "raw/*",
            "generic/_metadata_/_delivered_bom.json",
            "generic/_metadata_/_intermediate_Collect_delivered_bom.json",
        ],
        "Bundle": [
            "generic/_metadata_/_delivered_bom.json",
            "generic/_metadata_/_intermediate_Bundle_delivered_bom.json",
        ],
        "_finalize": ["generic/_metadata_/_delivered_bom.json"],
    }

    (products, stages) = _get_products(TRANSFER)

    assert stages == stages_expected
    assert products == products_expected


def test_generate_in_toto_layout(monkeypatch: MonkeyPatch, tmp_path: Path):
    product_owner_path = generate_and_write_rsa_keypair(filepath=str(tmp_path / "product_owner_key"), password="1234")
    functionary_path = generate_and_write_rsa_keypair(filepath=str(tmp_path / "functionary_key"), password="5678")

    def mock_dump_method(*args, **kwargs):  # pylint: disable=unused-argument
        return None

    monkeypatch.setattr(target=Metablock, name="dump", value=mock_dump_method)

    generate_in_toto_layout(TRANSFER, product_owner_path, functionary_path, project_owner_key_password="1234")


def test_hoppr_in_toto_links_no_attestation(tmp_path: Path):
    in_toto_link = HopprInTotoLinks(create_attestations=False, transfer=TRANSFER)
    in_toto_link.set_collection_root(str(tmp_path))
    in_toto_link.record_stage_start("test")
    in_toto_link.record_stage_stop("test")


def test_hoppr_in_toto_links_attestation(tmp_path: Path):
    functionary_path = generate_and_write_rsa_keypair(filepath=str(tmp_path / "functionary_key"), password="5678")

    in_toto_link = HopprInTotoLinks(
        create_attestations=True,
        transfer=TRANSFER,
        functionary_key_path=functionary_path,
        functionary_key_password="5678",
        metadata_directory=str(tmp_path),
    )
    in_toto_link.set_collection_root(str(tmp_path))
    in_toto_link.record_stage_start("Collect")
    in_toto_link.record_stage_stop("Collect")
