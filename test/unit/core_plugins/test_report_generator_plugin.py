"""
Test module for ReportGenerator class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=protected-access
# pylint: disable=unused-argument

import uuid

import pytest

from hoppr.core_plugins.report_generator import Report, ReportGenerator
from hoppr.models.manifest import Component
from hoppr.result import ResultStatus


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(name="TestComponent", purl="pkg:docker/something/else@1.2.3", type="file")  # type: ignore


@pytest.fixture(scope="function", params=[dict(plugin_class=ReportGenerator)])
def plugin_fixture(plugin_fixture: ReportGenerator) -> ReportGenerator:
    """
    Override and parametrize plugin_fixture to return ReportGenerator
    """

    return plugin_fixture


def test_get_version(plugin_fixture: ReportGenerator):
    """
    Test get_version method
    """

    assert len(plugin_fixture.get_version()) > 0


def test_generate_report(plugin_fixture: ReportGenerator):
    """
    Test generate_report method
    """

    plugin_fixture.generate_report()


@pytest.mark.parametrize(
    argnames=["result_list", "expected"],
    argvalues=[
        ([ResultStatus.FAIL, ResultStatus.FAIL], 0),
        ([ResultStatus.SUCCESS, ResultStatus.FAIL], 1),
        ([ResultStatus.SUCCESS, ResultStatus.SUCCESS], 2),
    ],
)
def test_get_successful_report_total(
    plugin_fixture: ReportGenerator, component: Component, result_list: list[ResultStatus], expected: int
):
    """
    Test _get_successful_report_total method
    """

    reports: list[Report] = []

    for result in result_list:
        reports.append(
            Report(uuid.uuid4(), "CollectDockerPlugin", "Collect", result, "", "process_component", component)
        )

    assert plugin_fixture._get_successful_report_total(reports) == expected


@pytest.mark.parametrize(
    argnames=["result_list", "expected"],
    argvalues=[
        ([ResultStatus.FAIL, ResultStatus.FAIL], "btn-danger"),
        ([ResultStatus.SUCCESS, ResultStatus.FAIL], "btn-warning"),
        ([ResultStatus.SUCCESS, ResultStatus.SUCCESS], "btn-success"),
    ],
)
def test_get_overall_status_button_class(
    plugin_fixture: ReportGenerator, component: Component, result_list: list[ResultStatus], expected: str
):
    """
    Test _get_overall_status_button_class method
    """

    reports: list[Report] = []

    for result in result_list:
        reports.append(
            Report(uuid.uuid4(), "CollectDockerPlugin", "Collect", result, "", "process_component", component)
        )

    assert plugin_fixture._get_overall_status_button_class(reports) == expected


@pytest.mark.parametrize(
    argnames=["sort_by", "expected"],
    argvalues=[("plugin", ["CollectMavenPlugin", "CollectAptPlugin", "TarBundle"]), ("stage", ["Collect", "Bundle"])],
)
def test_get_reports_by(plugin_fixture: ReportGenerator, component: Component, sort_by: str, expected: list):
    """
    Test _get_reports_by method
    """

    reports: list[Report] = [
        Report(uuid.uuid4(), "CollectMavenPlugin", "Collect", ResultStatus.SUCCESS, "", "process_component", component),
        Report(uuid.uuid4(), "CollectAptPlugin", "Collect", ResultStatus.FAIL, "", "process_component", component),
        Report(uuid.uuid4(), "TarBundle", "Bundle", ResultStatus.SUCCESS, "", "process_component"),
    ]

    sorted_reports = plugin_fixture._get_reports_by(sort_by, reports)

    assert list(sorted_reports.keys()) == expected
