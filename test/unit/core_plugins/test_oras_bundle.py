"""
Test module for OrasBundlePlugin class
"""

# pylint: disable=missing-function-docstring
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument
import os
import shutil

from pathlib import Path

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component, Scope, Type
from pytest import MonkeyPatch
from pytest_mock import MockerFixture

from hoppr.core_plugins.oras_bundle import OrasBundlePlugin
from hoppr.core_plugins.oras_registry import Registry
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.result import ResultStatus


@pytest.fixture(scope="function", params=[dict(plugin_class=OrasBundlePlugin)])
def plugin_fixture(plugin_fixture: OrasBundlePlugin) -> OrasBundlePlugin:
    """
    Override and parametrize plugin_fixture to return OrasBundlePlugin
    """
    return plugin_fixture


def test_no_comp_readerror(plugin_fixture: OrasBundlePlugin):
    assert len(plugin_fixture.get_version()) > 0


def test_oras_missing_client(plugin_fixture: OrasBundlePlugin, monkeypatch: MonkeyPatch):
    try:
        plugin_fixture.create_logger()
        plugin_fixture.get_oras_client("", "")
    except HopprPluginError:
        assert True


def test_oras_correct_client(plugin_fixture: OrasBundlePlugin, monkeypatch: MonkeyPatch):
    plugin_fixture.create_logger()
    registry_object = plugin_fixture.get_oras_client("username", "password")
    assert registry_object.headers['Authorization'] is not None


def test_get_files_from_root_dir_cdx(plugin_fixture: OrasBundlePlugin):
    plugin_fixture.create_logger()
    directory = Path('/tmp')
    file_list = [
        str(directory) + '/generic/_metadata_/_consolidated_bom.json',
        str(directory) + '/generic/_metadata_/_delivered_bom.json',
    ]
    test_root_path = str(directory) + '/generic'
    if os.path.exists(test_root_path):
        shutil.rmtree(test_root_path, ignore_errors=False, onerror=None)
    os.makedirs(str(directory) + '/generic/_metadata_/')
    for file in file_list:
        with open(file, 'a', encoding='utf-8'):
            print()
    val = plugin_fixture.get_files_from_root_dir(file_list=file_list, root_dir=directory)
    shutil.rmtree(test_root_path, ignore_errors=False, onerror=None)
    assert len(val) == 2


def test_verify_contents(plugin_fixture: OrasBundlePlugin, monkeypatch: MonkeyPatch):
    components = []
    component = Component(name='artifact', version='1.2.3', type=Type('library'))
    components.append(component)
    archives = []
    archive = {"path": '/test/artifact-1.2.3.jar'}
    archives.append(archive)
    try:
        plugin_fixture.create_logger()
        plugin_fixture.verify_contents(components=components, archives=archives)
    except HopprPluginError:
        assert True


def test_verify_contents_bad_result(plugin_fixture: OrasBundlePlugin):
    components = []
    component = Component(name='artifact', version='1.2.3', type=Type('library'))
    components.append(component)
    archives = []
    archive = {"path": ''}
    archives.append(archive)
    try:
        plugin_fixture.create_logger()
        plugin_fixture.verify_contents(components=components, archives=archives)
    except HopprPluginError:
        # Yes we this is correct behavior
        assert True

    archives = [{"path": 'artifact-1.2.3'}]
    component = Component(name='artifact', version='1.2.3', type=Type('library'), scope=Scope.excluded)
    components.append(component)
    archives = plugin_fixture.verify_contents(components=components, archives=archives)
    assert len(archives) == 0

    archives = [{"path": 'artifact-1.2.4'}]
    component = Component(name='artifact', version='1.2.3', type=Type('library'), scope=Scope.required)
    components.append(component)
    archives = plugin_fixture.verify_contents(components=components, archives=archives)
    assert len(archives) == 1


def test_get_files_from_root_dir(plugin_fixture: OrasBundlePlugin):
    directory = Path("docs")
    file_list = ['docs/CHANGELOG.md']
    val = len(plugin_fixture.get_files_from_root_dir(file_list=file_list, root_dir=directory))
    assert val > 0


def test_post_stage_process_success(
    plugin_fixture: OrasBundlePlugin,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
    mocker: MockerFixture,
):

    plugin_fixture.create_logger()
    mocker.patch.object(OrasBundlePlugin, "verify_contents", return_value=[True])
    mocker.patch.object(Registry, "push", return_value=[True])
    plugin_fixture.config = dict(
        oras_artifact_name="registry.gitlab-oras.com/hoppr/hoppr/test/oras-bundle",
        oras_artifact_version="1.2.3",
        oras_registry="registry.gitlab-oras.com",
    )

    try:
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        # Verify error on no-credentials
        assert True

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setenv(name="MOCH_PASS_ENV", value="testpassword")

    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.SUCCESS

    # Test missing artifact name
    try:
        plugin_fixture.config.pop('oras_artifact_name')
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        assert True

    # Test missing artifact name
    try:
        plugin_fixture.config['oras_artifact_name'] = "registry.gitlab.com/hoppr/hoppr/test/oras-bundle"
        plugin_fixture.config.pop('oras_artifact_version')
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        assert True

    # Test missing artifact name
    try:
        plugin_fixture.config['oras_artifact_version'] = "1.2.3"
        plugin_fixture.config.pop('oras_registry')
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        assert True

    try:
        plugin_fixture.config['oras_registry'] = "https://registry.gitlab.com/hoppr/hoppr/test/oras-bundle"
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        assert True

    try:
        plugin_fixture.config['oras_registry'] = "registry.gitlab.com:443/hoppr/hoppr/test/oras-bundle"
        result = plugin_fixture.post_stage_process()
    except HopprPluginError:
        assert True

    plugin_fixture.config = None
    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.FAIL


def test_post_stage_process_success_with_registry(
    plugin_fixture: OrasBundlePlugin,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
    mocker: MockerFixture,
):
    test_file = '/tmp/artifact-1.2.3.jar'
    plugin_fixture.create_logger()
    plugin_fixture.context.collect_root_dir = Path('.').resolve()
    mocker.patch.object(
        OrasBundlePlugin,
        "verify_contents",
        return_value=[{"path": test_file, "media_type": "test"}, {"path": 'not-a-file', "media_type": "test"}],
    )
    mocker.patch.object(Registry, "upload_blob", return_value=[True])
    mocker.patch.object(Registry, "_check_200_response", return_value=[True])
    mocker.patch.object(Registry, "do_request", return_value=[True])
    plugin_fixture.config = dict(
        oras_artifact_name="registry.gitlab.com/hoppr/hoppr/test/oras-bundle",
        oras_artifact_version="1.2.3",
        oras_registry="registry.gitlab.com",
    )
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setenv(name="MOCH_PASS_ENV", value="testpassword")

    with open(test_file, 'a', encoding='utf-8'):
        print()
    plugin_fixture.context.collect_root_dir = Path('./docs')
    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.SUCCESS

    plugin_fixture.config = dict(
        oras_artifact_name="registry.gitlab.com:443/hoppr/hoppr/test/oras-bundle",
        oras_artifact_version="1.2.3",
        oras_registry="registry.gitlab.com",
    )
    plugin_fixture.context.collect_root_dir = Path('./docs').resolve()
    result = plugin_fixture.post_stage_process()
    assert result.status == ResultStatus.SUCCESS
    os.remove(test_file)
