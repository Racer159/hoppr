"""
Test module for TarBundlePlugin class
"""

# pylint: disable=missing-function-docstring
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import os
import tarfile

from pathlib import Path

import pytest

from pytest import MonkeyPatch

from hoppr.core_plugins.bundle_tar import TarBundlePlugin


@pytest.fixture(scope="function", params=[dict(plugin_class=TarBundlePlugin)])
def plugin_fixture(plugin_fixture: TarBundlePlugin) -> TarBundlePlugin:
    """
    Override and parametrize plugin_fixture to return TarBundlePlugin
    """
    return plugin_fixture


def test_defaults(plugin_fixture: TarBundlePlugin, tmp_path: Path, monkeypatch: MonkeyPatch):
    with monkeypatch.context() as patch:
        patch.setattr(target=os.path, name="exists", value=lambda path: True)
        tar_file = tmp_path / "bundle.tar.gz"
        plugin_fixture.config = dict(tarfile_name=str(tar_file))
        result = plugin_fixture.post_stage_process()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"


def test_no_comp_readerror(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    def mock_tarfile_open(*args, **kwargs):
        raise tarfile.ReadError

    monkeypatch.setattr(target=tarfile, name="open", value=mock_tarfile_open)

    plugin_fixture.config = dict(tarfile_name="tar_file")
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert len(plugin_fixture.get_version()) > 0


def test_xz_filenotfound(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    def mock_tarfile_open(*args, **kwargs):
        raise FileNotFoundError

    monkeypatch.setattr(target=tarfile, name="open", value=mock_tarfile_open)

    plugin_fixture.config = dict(tarfile_name="tar_file", compression="lzma")
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_xz_permission(plugin_fixture: TarBundlePlugin, monkeypatch: MonkeyPatch):
    def mock_tarfile_open(*args, **kwargs):
        raise PermissionError

    monkeypatch.setattr(target=tarfile, name="open", value=mock_tarfile_open)

    plugin_fixture.config = dict(tarfile_name="tar_file", compression="bz")
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_bad_compression(plugin_fixture: TarBundlePlugin):
    plugin_fixture.config = dict(compression="bad")
    result = plugin_fixture.post_stage_process()
    assert result.is_fail(), f"Expected FAIL result, got {result}"


def test_fileexists_no_comp(plugin_fixture: TarBundlePlugin):
    plugin_fixture.config = dict(compression="none", tarfile_name="my.tarfile.tar")
    result = plugin_fixture.post_stage_process()
    assert result.is_success(), f"Expected SUCCESS result, got {result}"


def test_fileexists_noext(plugin_fixture: TarBundlePlugin):
    plugin_fixture.config = dict(tarfile_name="my.tarfile")
    result = plugin_fixture.post_stage_process()
    assert result.is_success(), f"Expected SUCCESS result, got {result}"
