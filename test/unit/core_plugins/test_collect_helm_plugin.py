"""
Test module for CollectHelmPlugin class
"""

# pylint: disable=redefined-outer-name

from __future__ import annotations

from pathlib import Path
from subprocess import CompletedProcess
from typing import Callable

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.core_plugins.collect_helm_plugin
import hoppr.plugin_utils
import hoppr.utils

from hoppr.core_plugins.collect_helm_plugin import CollectHelmPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component, Repository
from hoppr.models.types import PurlType
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = request.param if hasattr(request, "param") else "pkg:helm/something/else@1.2.3"
    return Component(name="TestHelmComponent", purl=purl, type="file")  # type: ignore[arg-type]


@pytest.fixture(scope="function")
def load_file_fixture(request: FixtureRequest) -> Callable:
    """
    Test yaml.safe_load fixture
    """

    def _load_file(input_file_path: Path) -> list | dict:  # pylint: disable=unused-argument
        data: list[dict[str, str]] = [
            {"url": "https://charts.hoppr.com/hoppr", "name": "hoppr"},
            {"url": "https://charts.hoppr.com/stable", "name": "stable"},
        ]

        return data if request.param == "raise" else {"repositories": data}

    return _load_file


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectHelmPlugin)])
def plugin_fixture(plugin_fixture: CollectHelmPlugin, monkeypatch: MonkeyPatch) -> CollectHelmPlugin:
    """
    Override and parametrize plugin_fixture to return CollectHelmPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.repositories[PurlType.HELM] = [
        Repository.parse_obj({"url": "https://somewhere.com", "description": ""})
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "expected_result"],
    argvalues=[
        (
            {"returncode": 0},
            Result.success(),
        ),
        (
            {"returncode": 1, "stderr": "b'404 Not Found\n'"},
            Result.fail(
                "Failed to download else version 1.2.3 helm chart from https://somewhere.com. Chart not found."
            ),
        ),
        (
            {"returncode": 1, "stderr": "b'Some other error\n'"},
            Result.fail("Failure after 3 attempts, final message Failed to download else version 1.2.3 helm chart"),
        ),
    ],
    ids=["command_success", "command_fail", "command_retry"],
    indirect=["completed_process_fixture"],
)
def test_collect_helm(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectHelmPlugin,
    run_command_fixture: Callable[..., CompletedProcess],
    find_credentials_fixture: CredentialRequiredService,
    component: Component,
    expected_result: Result,
    monkeypatch: MonkeyPatch,
):
    """
    Test CollectHelmPlugin.collect method
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.status == expected_result.status
    assert collect_result.message.startswith(expected_result.message)


@pytest.mark.parametrize(
    argnames=["context_fixture", "load_file_fixture"],
    argvalues=[({"strict_repos": False}, "load"), ({"strict_repos": False}, "raise")],
    indirect=True,
)
def test_helm_no_strict(
    context_fixture: HopprContext,
    load_file_fixture: list[dict[str, str]],
    monkeypatch: MonkeyPatch,
):
    """
    Test plugin creation with --no-strict flag
    """
    monkeypatch.setattr(target=context_fixture, name="strict_repos", value=False)
    monkeypatch.setattr(target=Path, name="exists", value=lambda self: True)
    monkeypatch.setattr(target=hoppr.utils, name="load_file", value=load_file_fixture)

    plugin = CollectHelmPlugin(context=context_fixture, config={"helm_command": "helm"})
    assert plugin.system_repositories == ["https://charts.hoppr.com/hoppr", "https://charts.hoppr.com/stable"]
