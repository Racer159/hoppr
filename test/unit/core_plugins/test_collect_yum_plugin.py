"""Test module for CollectYumPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument
# pylint: disable=duplicate-code

from __future__ import annotations

from subprocess import CompletedProcess

import pytest

from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_yum_plugin import CollectYumPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component
from hoppr.result import Result


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectYumPlugin)])
def plugin_fixture(plugin_fixture: CollectYumPlugin) -> CollectYumPlugin:
    """
    Override and parametrize plugin_fixture to return CollectYumPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(name="TestComponent", purl="pkg:rpm/pidgin@2.7.9-5.el6.2?arch=x86_64", type="file")  # type: ignore


def get_repos(comp: Component) -> list[str]:
    """
    Mock _get_repos method
    """
    return ["https://somewhere.com"]


@pytest.mark.parametrize(
    argnames="config_fixture", argvalues=[dict(yumdownloader_command="yumdownloader")], indirect=True
)
@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0, stdout=b"https://somewhere.com")], indirect=True
)
def test_collect_yum_success(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Yum Collector
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test_collect_yum_fail(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Yum Collector
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message yumdownloader failed to locate package for"
    )


def test_get_version(plugin_fixture: CollectYumPlugin):
    """
    Test yum collector version
    """
    assert len(plugin_fixture.get_version()) > 0


def test_collect_yum_command_not_found(
    plugin_fixture: CollectYumPlugin,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test yum collector collect with missing command
    """

    def get_missing_command(*args, **kwargs) -> Result:
        return Result.fail("[mock] command not found")

    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=["https://somewhere.com"])
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=get_missing_command)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


@pytest.mark.parametrize(
    argnames="config_fixture", argvalues=[dict(yumdownloader_command="yumdownloader")], indirect=True
)
@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0, stdout=b"https://elsewhere.com")], indirect=True
)
def test_collect_yum_bad_repo(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: dict[str, str],
    run_command_fixture: CompletedProcess,
):

    """
    Test yum collector collect with mismatched repo urls
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.message.startswith("Yum download url does not match requested url")


@pytest.mark.parametrize(
    argnames="config_fixture", argvalues=[dict(yumdownloader_command="yumdownloader")], indirect=True
)
@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[
        dict(returncode=1, stdout=b"https://somewhere.com"),
    ],
    indirect=True,
)
def test_collect_yum_fail_download(
    plugin_fixture: CollectYumPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test yum collector collect when file exists but cannot be downloaded
    """
    mock_response = [
        dict(returncode=0, stdout="https://somewhere.com"),
        dict(returncode=1, stdout="https://somewhere.com"),
        dict(returncode=0, stdout="https://somewhere.com"),
        dict(returncode=1, stdout="https://somewhere.com"),
        dict(returncode=0, stdout="https://somewhere.com"),
        dict(returncode=1, stdout="https://somewhere.com"),
    ]

    class MockResponse:
        """
        Mock Response that uses returncode instead of statuscode
        """

        returncode = 404
        stdout = bytes("not found", "utf-8")

        def __init__(self, returncode: int, stdout: str) -> None:
            self.returncode = returncode
            self.stdout = bytes(stdout, "utf-8")

        @property
        def get_stdout(self) -> bytes:
            """
            get stdout of response
            """
            return self.stdout

        @property
        def get_returncode(self) -> int:
            """
            get returncode of response
            """
            return self.returncode

    def run_command(*args, **kwargs):
        response = mock_response.pop()
        return MockResponse(int(response["returncode"]), str(response["stdout"]))

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download Yum artifact")
