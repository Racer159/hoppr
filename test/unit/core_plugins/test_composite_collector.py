"""
Test module for CompositeCollector class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from packageurl import PackageURL
from pytest import FixtureRequest
from pytest_mock import MockerFixture

from hoppr.constants import ConfigKeys
from hoppr.core_plugins.collect_dnf_plugin import CollectDnfPlugin
from hoppr.core_plugins.collect_nexus_search import CollectNexusSearch
from hoppr.core_plugins.composite_collector import CompositeCollector
from hoppr.exceptions import HopprPluginError
from hoppr.models import HopprContext
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl_string = getattr(request, "param", "pkg:rpm/test_component@0.1.2")
    purl = PackageURL.from_string(purl_string)
    return Component.parse_obj(dict(name=purl.name, version=purl.version, purl=purl_string, type="file"))


@pytest.fixture
def config_fixture(config_fixture: dict[ConfigKeys, list[dict[str, str]]]) -> dict[ConfigKeys, list[dict[str, str]]]:
    """
    Test plugin config fixture
    """
    return {
        ConfigKeys.PLUGINS: [
            {"name": "hoppr.core_plugins.collect_dnf_plugin"},
            {"name": "hoppr.core_plugins.collect_nexus_search"},
        ]
    }


@pytest.fixture(scope="function", params=[dict(plugin_class=CompositeCollector)])
def plugin_fixture(plugin_fixture: CompositeCollector) -> CompositeCollector:
    """
    Override and parametrize plugin_fixture to return CompositeCollector
    """
    return plugin_fixture


def test_get_version(plugin_fixture: CollectNexusSearch):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


def test_composite_pre_no_config(context_fixture: HopprContext):
    """
    Test CompositeCollector plugin creation without config raises HopprPluginError
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector(context=context_fixture, config=None)


def test_composite_pre_no_children(context_fixture: HopprContext):
    """
    Test CompositeCollector plugin creation with no child plugins raises HopprPluginError
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector(context=context_fixture, config={ConfigKeys.PLUGINS: []})


def test_composite_pre_success(plugin_fixture: CompositeCollector, mocker: MockerFixture):
    """
    Test CompositeCollector.pre_stage_process method: success
    """
    mock_dnf_pre = mocker.patch.object(CollectDnfPlugin, "pre_stage_process", return_value=Result.success())
    mock_nexus_pre = mocker.patch.object(CollectNexusSearch, "pre_stage_process", return_value=Result.success())

    composite_result = plugin_fixture.pre_stage_process()
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_pre.call_count == 1
    assert mock_nexus_pre.call_count == 1


def test_composite_pre_fail(plugin_fixture: CompositeCollector, mocker: MockerFixture):
    """
    Test CompositeCollector.pre_stage_process method: fail
    """
    mock_dnf_pre = mocker.patch.object(CollectDnfPlugin, "pre_stage_process", return_value=Result.success())
    mock_nexus_pre = mocker.patch.object(CollectNexusSearch, "pre_stage_process", return_value=Result.fail())

    composite_result = plugin_fixture.pre_stage_process()
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_pre.call_count == 1
    assert mock_nexus_pre.call_count == 1


def test_composite_comp_success(plugin_fixture: CompositeCollector, component: Component, mocker: MockerFixture):
    """
    Test CompositeCollector.process_component method: success
    """
    mock_dnf_comp = mocker.patch.object(CollectDnfPlugin, "process_component", return_value=Result.success())
    mock_nexus_comp = mocker.patch.object(CollectNexusSearch, "process_component", return_value=Result.fail())

    composite_result = plugin_fixture.process_component(component)
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_comp.call_count == 1
    assert mock_nexus_comp.call_count == 0


def test_composite_comp_fail(plugin_fixture: CompositeCollector, component: Component, mocker: MockerFixture):
    """
    Test CompositeCollector.process_component method: fail
    """
    mock_dnf_comp = mocker.patch.object(CollectDnfPlugin, "process_component", return_value=Result.fail())
    mock_nexus_comp = mocker.patch.object(CollectNexusSearch, "process_component", return_value=Result.fail())

    composite_result = plugin_fixture.process_component(component)
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_comp.call_count == 1
    assert mock_nexus_comp.call_count == 1


def test_composite_post_success(plugin_fixture: CompositeCollector, mocker: MockerFixture):
    """
    Test CompositeCollector.post_stage_process method: success
    """
    mock_dnf_post = mocker.patch.object(CollectDnfPlugin, "post_stage_process", return_value=Result.success())
    mock_nexus_post = mocker.patch.object(CollectNexusSearch, "post_stage_process", return_value=Result.success())

    composite_result = plugin_fixture.post_stage_process()
    assert composite_result.is_success(), f"Expected SUCCESS result, got {composite_result}"
    assert mock_dnf_post.call_count == 1
    assert mock_nexus_post.call_count == 1


def test_composite_post_fail(plugin_fixture: CompositeCollector, mocker: MockerFixture):
    """
    Test CompositeCollector.post_stage_process method: fail
    """
    mock_dnf_post = mocker.patch.object(CollectDnfPlugin, "post_stage_process", return_value=Result.success())
    mock_nexus_post = mocker.patch.object(CollectNexusSearch, "post_stage_process", return_value=Result.fail())

    composite_result = plugin_fixture.post_stage_process()
    assert composite_result.is_fail(), f"Expected FAIL result, got {composite_result}"
    assert mock_dnf_post.call_count == 1
    assert mock_nexus_post.call_count == 1


def test_get_attestation_prods_config(config_fixture):
    """
    Test getting attestation products from user config
    """
    prods = CompositeCollector.get_attestation_products(config_fixture)

    assert prods == ["rpm/*"]


def test_get_attestation_prods_no_config():
    """
    Test getting attestation products without a config
    """
    with pytest.raises(HopprPluginError):
        CompositeCollector.get_attestation_products()
