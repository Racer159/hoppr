"""
Test module for CollectAptPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import test

from io import BytesIO, TextIOWrapper
from pathlib import Path
from subprocess import CalledProcessError, CompletedProcess
from typing import Optional, Tuple

import jc
import pytest

from packageurl import PackageURL
from pytest import MonkeyPatch

from hoppr.core_plugins.collect_apt_plugin import CollectAptPlugin
from hoppr.exceptions import HopprPluginError
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Repository
from hoppr.models.manifest import Component
from hoppr.result import Result

sbom_path = Path(test.__file__).parent / "resources" / "bom" / "int_apt_bom.json"


def mock__get_component_download_info(purl: PackageURL) -> Tuple[str, str]:
    """
    Mock the _get_component_download_info method
    """
    return (
        "http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_1.0.0_amd64.deb",
        "hippo_1.0.0_amd64.deb",
    )


def mock__get_download_url_path(purl: PackageURL) -> str:
    """
    Mock the _get_download_url_path method
    """
    return "pool/main/h/hippo/hippo_1.0.0_amd64.deb"


def mock__get_found_repo(found_url: str) -> str:
    """
    Mock the _get_found_url method
    """
    return "http://archive.hoppr-test.com/ubuntu"


@pytest.fixture(name="component")
def component_fixture():
    """
    Test Component fixture
    """
    return Component(
        name="TestComponent", purl="pkg:deb/test-artifact@1:1.0.0?arch=all", type="application"  # type: ignore
    )


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectAptPlugin)])
def plugin_fixture(plugin_fixture: CollectAptPlugin) -> CollectAptPlugin:
    """
    Override and parametrize plugin_fixture to return CollectAptPlugin
    """
    return plugin_fixture


@pytest.mark.parametrize(
    argnames=["purl_string", "expected"],
    argvalues=[
        ("pkg:deb/ubuntu/test-artifact-1@1.0.0?arch=all", "test-artifact-1:all=1.0.0"),
        ("pkg:deb/ubuntu/test-artifact-2@3.4.5-1ubuntu1.6?arch=amd64", "test-artifact-2:amd64=3.4.5-1ubuntu1.6"),
    ],
)
def test__artifact_string(plugin_fixture: CollectAptPlugin, purl_string: str, expected: str):
    """
    Test _artifact_string method
    """
    purl = PackageURL.from_string(purl_string)
    assert plugin_fixture._artifact_string(purl=purl) == expected  # type: ignore


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0), dict(returncode=1)], indirect=True
)
def test__download_component(
    plugin_fixture: CollectAptPlugin,
    monkeypatch: MonkeyPatch,
    completed_process_fixture: CompletedProcess,
    run_command_fixture: CompletedProcess,
):
    """
    Test _download_component method
    """
    purl = PackageURL.from_string(purl="pkg:deb/test-artifact@1:1.0.0?arch=all")

    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    if completed_process_fixture.returncode == 0:
        plugin_fixture._download_component(purl=purl)  # type: ignore
    else:
        with pytest.raises(expected_exception=CalledProcessError):
            plugin_fixture._download_component(purl=purl)  # type: ignore


@pytest.mark.parametrize(
    argnames="completed_process_fixture",
    argvalues=[
        dict(returncode=0, stdout=b""),
        dict(returncode=1),
        dict(
            returncode=0,
            stdout=(
                b"'http://archive.ubuntu.com/ubuntu/pool/main/g/git/git_2.34.1-1ubuntu1.5_amd64.deb' "
                b"git_1%3a2.34.1-1ubuntu1.5_amd64.deb"
            ),
        ),
    ],
    indirect=True,
)
def test__get_component_download_info(
    plugin_fixture: CollectAptPlugin,
    monkeypatch: MonkeyPatch,
    completed_process_fixture: CompletedProcess,
    run_command_fixture: CompletedProcess,
):
    """
    Test _get_component_download_info method
    """
    plugin_fixture.create_logger()
    purl = PackageURL.from_string(purl="pkg:deb/test-artifact@1:1.0.0?arch=all")

    if completed_process_fixture.returncode == 1:
        with monkeypatch.context() as patch:
            patch.setattr(
                target=plugin_fixture,
                name="run_command",
                value=run_command_fixture,
            )

            with pytest.raises(expected_exception=CalledProcessError):
                plugin_fixture._get_component_download_info(purl=purl)  # type: ignore
    elif completed_process_fixture.returncode == 0 and completed_process_fixture.stdout.decode(encoding="utf-8") == "":
        with monkeypatch.context() as patch:
            patch.setattr(
                target=plugin_fixture,
                name="run_command",
                value=run_command_fixture,
            )

            with pytest.raises(expected_exception=HopprPluginError):
                plugin_fixture._get_component_download_info(purl=purl)  # type: ignore
    else:
        with monkeypatch.context() as patch:
            patch.setattr(
                target=plugin_fixture,
                name="run_command",
                value=run_command_fixture,
            )

            download_info = plugin_fixture._get_component_download_info(purl=purl)  # type: ignore

            assert download_info == (
                "http://archive.ubuntu.com/ubuntu/pool/main/g/git/git_2.34.1-1ubuntu1.5_amd64.deb",
                "git_1%3a2.34.1-1ubuntu1.5_amd64.deb",
            )


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test__get_download_url_path_apt_cache_fail(
    plugin_fixture: CollectAptPlugin, monkeypatch: MonkeyPatch, run_command_fixture: CompletedProcess
):
    """
    Test _get_download_url_path method
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    with pytest.raises(expected_exception=CalledProcessError):
        purl = PackageURL.from_string(purl="pkg:deb/test-artifact@1:1.0.0?arch=all")
        plugin_fixture._get_download_url_path(purl=purl)  # type: ignore


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0, stdout=b"")], indirect=True
)
def test__get_download_url_path_parse_fail(
    plugin_fixture: CollectAptPlugin, run_command_fixture: CompletedProcess, monkeypatch: MonkeyPatch
):
    """
    Test _get_download_url_path method
    """

    def jc_parse(parser_mod_name: str, data: str):
        return dict(Filename=None)

    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=jc, name="parse", value=jc_parse)

    with pytest.raises(expected_exception=TypeError):
        purl = PackageURL.from_string(purl="pkg:deb/test-artifact@1:1.0.0?arch=all")
        plugin_fixture._get_download_url_path(purl=purl)  # type: ignore


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0, stdout=b"")], indirect=True
)
def test__get_download_url_path_success(
    plugin_fixture: CollectAptPlugin, run_command_fixture: CompletedProcess, monkeypatch: MonkeyPatch
):
    """
    Test _get_download_url_path_success method success condition
    """

    def jc_parse(parser_mod_name: str, data: str):
        return dict(Filename="test-artifact-1_1.0.0_all.deb")

    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=jc, name="parse", value=jc_parse)

    purl = PackageURL.from_string(purl="pkg:deb/test-artifact@1:1.0.0?arch=all")
    url_path = plugin_fixture._get_download_url_path(purl=purl)  # type: ignore

    assert url_path == "test-artifact-1_1.0.0_all.deb"


@pytest.mark.parametrize(
    argnames=["found_url", "expected"],
    argvalues=[
        (
            "http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_1.0.0_amd64.deb",
            "http://archive.hoppr-test.com/ubuntu",
        ),
        (
            "http://archive.hoppr-fail.com/ubuntu/pool/main/h/hippo/hippo_1.0.0_amd64.deb",
            None,
        ),
    ],
)
def test__get_found_repo(plugin_fixture: CollectAptPlugin, found_url: str, expected: Optional[str]):
    """
    Test _get_found_repo method
    """
    plugin_fixture.manifest_repos = [
        "http://archive.hoppr-test.com/ubuntu",
        "http://archive.hoppr-test.com/debian",
    ]

    found_repo = plugin_fixture._get_found_repo(found_url=found_url)
    assert found_repo == expected


def test__populate_apt_folder_structure_success(
    plugin_fixture: CollectAptPlugin, monkeypatch: MonkeyPatch, tmp_path: Path
):
    """
    Test _populate_apt_folder_structure method
    """

    def mock_path_method(*args, **kwargs):
        return None

    monkeypatch.setattr(target=Path, name="mkdir", value=mock_path_method)
    monkeypatch.setattr(target=Path, name="touch", value=mock_path_method)

    plugin_fixture._populate_apt_folder_structure(apt_path=tmp_path, path_dict={"etc": {"apt": {"sources.list": None}}})


def test__populate_apt_folder_structure_fail(plugin_fixture: CollectAptPlugin, monkeypatch: MonkeyPatch):
    """
    Test _populate_apt_folder_structure method
    """

    def mock_path_method(*args, **kwargs):
        return None

    monkeypatch.setattr(target=Path, name="mkdir", value=mock_path_method)
    monkeypatch.setattr(target=Path, name="touch", value=mock_path_method)

    with pytest.raises(expected_exception=TypeError):
        plugin_fixture._populate_apt_folder_structure(
            apt_path=Path("/tmp"),
            path_dict={"etc": {"apt": {"sources.list": "WRONG TYPE"}}},
        )


def test__populate_auth_conf(
    plugin_fixture: CollectAptPlugin,
    find_credentials_fixture: CredentialRequiredService,
    monkeypatch: MonkeyPatch,
    tmp_path: Path,
):
    """
    Test _populate_auth_conf method
    """

    def mock_chmod(file: Path, mode: int):
        return None

    def mock_open(*args, **kwargs):
        buffer = BytesIO(initial_bytes=b"")
        wrapper = TextIOWrapper(buffer=buffer)
        wrapper.write = lambda x: 0
        return wrapper

    auth_conf = tmp_path / ".hoppr-apt" / "etc" / "apt" / "auth.conf"
    repo_list = [
        Repository.parse_obj(
            dict(
                url="http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_1.0.0_amd64.deb",
                description="Mock repo 1",
            )
        ),
        Repository.parse_obj(
            dict(
                url="http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_2.0.0_amd64.deb",
                description="Mock repo 2",
            )
        ),
    ]

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=Path, name="chmod", value=mock_chmod)
    monkeypatch.setattr(target=Path, name="open", value=mock_open)

    plugin_fixture._populate_auth_conf(repo_list=repo_list, file=auth_conf)


def test__populate_sources_list(plugin_fixture: CollectAptPlugin, monkeypatch: MonkeyPatch, tmp_path: Path):
    """
    Test _populate_sources_list method
    """

    def mock_open_read(*args, **kwargs):
        buffer = BytesIO(initial_bytes=b"VERSION_CODENAME=jammy\n")
        wrapper = TextIOWrapper(buffer=buffer)
        return wrapper

    sources_list = tmp_path / ".hoppr-apt" / "etc" / "apt" / "sources.list"
    repo_list = [
        "http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_1.0.0_amd64.deb",
        "http://archive.hoppr-test.com/ubuntu/pool/main/h/hippo/hippo_2.0.0_amd64.deb",
    ]

    monkeypatch.setattr(target=Path, name="open", value=mock_open_read)

    plugin_fixture._populate_sources_list(repo_list=repo_list, file=sources_list)


def test__repo_proxy(plugin_fixture: CollectAptPlugin, monkeypatch: MonkeyPatch):
    """
    Test _repo_proxy method
    """
    monkeypatch.setenv(name="http_proxy", value="http://proxy.hoppr-test.com")
    monkeypatch.setenv(name="https_proxy", value="http://proxy.hoppr-test.com")
    monkeypatch.setenv(name="no_proxy", value="127.0.0.1,localhost,local,hoppr-test.com")

    plugin_fixture.manifest_repos = ["http://archive.hoppr-test.com/ubuntu"]
    proxy_args = plugin_fixture._repo_proxy()

    assert "--option=Acquire::http::Proxy::archive.hoppr-test.com=DIRECT" in proxy_args
    assert "--option=Acquire::https::Proxy::archive.hoppr-test.com=DIRECT" in proxy_args


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test_collect__download_component_exception(
    plugin_fixture: CollectAptPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: download component raises exception
    """

    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_component_download_info", value=mock__get_component_download_info
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_download_url_path", value=mock__get_download_url_path)
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=mock__get_found_repo)
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.success)

    result = plugin_fixture.process_component(comp=component)
    assert result.is_fail()


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0, stdout=b"")], indirect=True
)
def test_collect__get_component_download_info_exception(
    plugin_fixture: CollectAptPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: failure to get component download info raises exception
    """
    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    result = plugin_fixture.process_component(comp=component)
    assert result.is_fail()


def test_collect__get_found_repo_fail(plugin_fixture: CollectAptPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test collect method: download URL not in manifest repos fails
    """
    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_component_download_info", value=mock__get_component_download_info
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_download_url_path", value=mock__get_download_url_path)
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=lambda found_url: None)

    result = plugin_fixture.process_component(comp=component)
    assert result.is_fail()


def test_collect_check_purl_specified_url_fail(
    plugin_fixture: CollectAptPlugin, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test collect method: repository URL doesn't match repo URL
    """

    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_component_download_info", value=mock__get_component_download_info
    )
    monkeypatch.setattr(target=plugin_fixture, name="_get_download_url_path", value=mock__get_download_url_path)
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=mock__get_found_repo)
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.fail)

    result = plugin_fixture.process_component(comp=component)
    assert result.is_fail()


@pytest.mark.parametrize(argnames="strict_repos", argvalues=[True, False])
def test_collect_success(
    plugin_fixture: CollectAptPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
    strict_repos: bool,
):
    """
    Test collect method: success with `--strict` and `--no-strict`
    """

    def mock_shutil_move(src: Path, dst: Path) -> Path:
        return Path(".") / "mock_pkg.deb"

    plugin_fixture.context.strict_repos = strict_repos

    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_found_repo", value=mock__get_found_repo)
    monkeypatch.setattr(target=plugin_fixture, name="_get_download_url_path", value=mock__get_download_url_path)
    monkeypatch.setattr(target="shutil.move", name=mock_shutil_move)

    result = plugin_fixture.process_component(comp=component)
    assert result.is_success()


def test_get_version(plugin_fixture: CollectAptPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(argnames="strict_repos", argvalues=[True, False])
def test_pre_stage_process_success(
    plugin_fixture: CollectAptPlugin,
    monkeypatch: MonkeyPatch,
    run_command_fixture: CompletedProcess,
    strict_repos: bool,
):
    """
    Test pre_stage_process method with `--strict` and `--no-strict`
    """

    def mock_copy(*args, **kwargs):
        return Path("mock_copy")

    def mock_populate(*args, **kwargs):
        return None

    plugin_fixture.context.strict_repos = strict_repos

    monkeypatch.setattr("shutil.copyfile", mock_copy)
    monkeypatch.setattr("shutil.copytree", mock_copy)

    monkeypatch.setattr(target=plugin_fixture, name="_populate_apt_folder_structure", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_auth_conf", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_sources_list", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)

    result = plugin_fixture.pre_stage_process()
    assert result.is_success()


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test_pre_stage_process_apt_update_fail(
    plugin_fixture: CollectAptPlugin, run_command_fixture: CompletedProcess, monkeypatch: MonkeyPatch
):
    """
    Test pre_stage_process method `apt update` failure
    """

    def mock_populate(*args, **kwargs):
        return None

    monkeypatch.setattr(target=plugin_fixture, name="_populate_apt_folder_structure", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_auth_conf", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="_populate_sources_list", value=mock_populate)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target="hoppr.plugin_utils.check_for_missing_commands", name=Result.success)

    result = plugin_fixture.pre_stage_process()

    assert result.is_fail()
    assert result.message == "Failed to populate Apt cache."
