"""
Test module for DeltaSbom class
"""
from __future__ import annotations

import io
import tarfile
import tempfile

from copy import deepcopy
from pathlib import Path

import pytest

from packageurl import PackageURL
from pytest import MonkeyPatch

from hoppr.core_plugins.delta_sbom import DeltaSbom
from hoppr.exceptions import HopprError
from hoppr.models.manifest import Component, Manifest, Repositories, Sbom

# pylint: disable=missing-function-docstring
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument
# pylint: disable=consider-using-with


@pytest.fixture(scope="function", params=[dict(plugin_class=DeltaSbom)])
def plugin_fixture(plugin_fixture: DeltaSbom) -> DeltaSbom:
    """
    Override and parametrize plugin_fixture to return DeltaSbom
    """
    return plugin_fixture


def test_get_version(plugin_fixture: DeltaSbom):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


def mock_component(purl_str: str, hashes: list | None = None):
    for hash_item in hashes or []:
        hash_item["content"] = hash_item["content"].rjust(32, '0')

    purl: PackageURL = PackageURL.from_string(purl_str)  # pyright: ignore[reportGeneralTypeIssues]

    return Component(
        name="Test_component",
        purl=purl_str,
        type="file",  # type: ignore[arg-type]
        hashes=hashes,
        version=purl.version,
        components=[],
    )


def test_prestage_success(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])  # type: ignore
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@latest"))

    new_bom = deepcopy(prev_bom)
    new_bom.components.append(mock_component("pkg:docker/delta@4.5.6"))  # type: ignore

    plugin_fixture.config = {"previous": "previous_source_location"}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *args, **kwargs: True)
        patch.setattr(target=DeltaSbom, name="_get_previous_bom", value=lambda *args, **kwargs: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        # "beta" is in returned bom because "latest" may have changed
        assert len(result.return_obj.components) == 2
        assert result.return_obj.components[0].purl == "pkg:docker/beta@latest"
        assert result.return_obj.components[1].purl == "pkg:docker/delta@4.5.6"


def test_prestage_success_with_override(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    plugin_fixture.context.previous_delivery = "cli-override-location"

    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])  # type: ignore
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@latest"))

    new_bom = deepcopy(prev_bom)
    new_bom.components.append(mock_component("pkg:docker/delta@4.5.6"))  # type: ignore

    plugin_fixture.config = {"previous": "previous_source_location"}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *args, **kwargs: True)
        patch.setattr(target=DeltaSbom, name="_get_previous_bom", value=lambda *args, **kwargs: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        # "beta" is in returned bom because "latest" may have changed
        assert len(result.return_obj.components) == 2
        assert result.return_obj.components[0].purl == "pkg:docker/beta@latest"
        assert result.return_obj.components[1].purl == "pkg:docker/delta@4.5.6"


def test_prestage_fail_no_change(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    prev_bom = Sbom(specVersion="1.4", version=1, bomFormat="CycloneDX", components=[])  # type: ignore
    prev_bom.components.append(mock_component("pkg:docker/path/to/image/image_name@1.2.3"))
    prev_bom.components.append(mock_component("pkg:docker/beta@4.5.6"))

    new_bom = deepcopy(prev_bom)

    plugin_fixture.config = {"previous": "previous_source_location"}

    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture.context, name="delivered_sbom", value=new_bom)
        patch.setattr(target=Path, name="exists", value=lambda *args, **kwargs: True)
        patch.setattr(target=DeltaSbom, name="_get_previous_bom", value=lambda *args, **kwargs: prev_bom)

        result = plugin_fixture.pre_stage_process()

        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "No components updated since \"previous_source_location\"."


def test_prestage_file_not_found(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    plugin_fixture.config = {"previous": "previous_source_location"}

    monkeypatch.setattr(target=Path, name="exists", value=lambda *args, **kwargs: False)

    result = plugin_fixture.pre_stage_process()

    assert result.is_fail(), f"Expected FAIL result, got {result}"
    assert result.message == "Previous source file \"previous_source_location\" not found."


def test_prestage_noconfig(plugin_fixture: DeltaSbom):
    result = plugin_fixture.pre_stage_process()

    assert result.is_success(), f"Expected SUCCESS result, got {result}"
    assert (
        result.message == "No previously delivered bundle specified for delta bundle. All components will be delivered"
    )


def test_get_bom_manifest(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    def mock_load_manifest(*args, **kwargs):
        return Manifest(kind="Manifest", metadata=None, schemaVersion="v1", repositories=Repositories())

    monkeypatch.setattr(target=Manifest, name="load", value=mock_load_manifest)

    bom = plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access
    assert bom is not None


def test_get_bom_tar_success(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    def mock_load_manifest(*args, **kwargs):
        raise TypeError

    def mock_tar_extraction(*args, **kwargs):
        sbom_bytestr = b"""
            {
                "bomFormat": "CycloneDX",
                "specVersion": "1.4",
                "version": 1,
                "components": []
            }
        """

        return io.BytesIO(sbom_bytestr)

    with tempfile.TemporaryDirectory() as temp_dir:

        monkeypatch.setattr(target=Manifest, name="load", value=mock_load_manifest)
        monkeypatch.setattr(
            target=tarfile,
            name="open",
            value=lambda *args, **kwargs: tarfile.TarFile(name=f"{temp_dir}/testtarfilename", mode="w"),
        )
        monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=mock_tar_extraction)

        bom = plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access
        assert bom is not None


def test_get_bom_tar_empty_buffer(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Testing a type check that was added only to satisfy mypy
    """

    def mock_load_manifest(*args, **kwargs):
        raise TypeError

    with tempfile.TemporaryDirectory() as temp_dir:

        monkeypatch.setattr(target=Manifest, name="load", value=mock_load_manifest)
        monkeypatch.setattr(
            target=tarfile,
            name="open",
            value=lambda *args, **kwargs: tarfile.TarFile(name=f"{temp_dir}/testtarfilename", mode="w"),
        )
        monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=lambda *args, **kwargs: None)

        with pytest.raises(HopprError):
            plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access


def test_get_bom_tar_not_dict(plugin_fixture: DeltaSbom, monkeypatch: MonkeyPatch):
    """
    Testing a type check that was added only to satisfy mypy
    """

    def mock_load_manifest(*args, **kwargs):
        raise TypeError

    def mock_tar_extraction(*args, **kwargs):
        sbom_bytestr = b"""
            ["sbom",
            "isn't",
            "a",
            "list"]
        """

        return io.BytesIO(sbom_bytestr)

    with tempfile.TemporaryDirectory() as temp_dir:

        monkeypatch.setattr(target=Manifest, name="load", value=mock_load_manifest)
        monkeypatch.setattr(
            target=tarfile,
            name="open",
            value=lambda *args, **kwargs: tarfile.TarFile(name=f"{temp_dir}/testtarfilename", mode="w"),
        )
        monkeypatch.setattr(target=tarfile.TarFile, name="extractfile", value=mock_tar_extraction)

        with pytest.raises(HopprError):
            plugin_fixture._get_previous_bom("source")  # pylint: disable=protected-access


@pytest.mark.parametrize(
    "new_comp, prev_comp, expected",
    [
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            True,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@latest"),
            mock_component("pkg:docker/path/to/image/image_name@latest"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@latest", [{"alg": "SHA-1", "content": "1701D"}]),
            mock_component("pkg:docker/path/to/image/image_name@latest", [{"alg": "SHA-1", "content": "1701D"}]),
            True,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image-name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:generic/path/to/image/image_name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.4"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/from/image/image_name@1.2.3"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3#subpath"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3"),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3?arch=linux"),
            False,
        ),
        (
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701E"}]),
            False,
        ),
        (
            mock_component(
                "pkg:docker/path/to/image/image_name@1.2.3",
                [{"alg": "SHA-1", "content": "1701D"}, {"alg": "SHA-256", "content": "1701D"}],
            ),
            mock_component("pkg:docker/path/to/image/image_name@1.2.3", [{"alg": "SHA-1", "content": "1701D"}]),
            True,
        ),
    ],
)
def test_comp_match(plugin_fixture: DeltaSbom, new_comp, prev_comp, expected):

    print(f"New Component:  purl: {new_comp.purl}, hashes: {new_comp.hashes}")
    print(f"Prev Component: purl: {prev_comp.purl}, hashes: {prev_comp.hashes}")

    match = plugin_fixture._component_match(new_comp, prev_comp)  # pylint: disable=protected-access

    assert match is expected
