"""
Test module for CollectMavenPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from __future__ import annotations

import os

from pathlib import Path
from subprocess import CompletedProcess

import pytest

from pytest import FixtureRequest, MonkeyPatch

import hoppr.plugin_utils

from hoppr.core_plugins.collect_maven_plugin import CollectMavenPlugin
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component, Repository
from hoppr.models.types import PurlType
from hoppr.result import Result

SETTINGS_XML = """<settings>
    <profiles>
        <profile>
            <id>hoppr-test-profile</id>
            <repositories>
                <repository>
                    <id>hoppr-test-repo-1</id>
                    <name>Hoppr Test 1</name>
                    <url>https://somewhere.com</url>
                </repository>
                <repository>
                    <id>hoppr-test-repo-2</id>
                    <name>Hoppr Test 2</name>
                    <url>https://somewhere.else.com</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
"""


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest):
    """
    Test Component fixture
    """
    purl = getattr(request, "param", "pkg:maven/something/else@1.2.3")
    return Component(name="TestMavenComponent", purl=purl, type="file")  # type: ignore


@pytest.fixture(scope="function")
def context_fixture(context_fixture: HopprContext, monkeypatch: MonkeyPatch) -> HopprContext:
    """
    Test Context fixture
    """
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML)

    return context_fixture


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectMavenPlugin)])
def plugin_fixture(
    plugin_fixture: CollectMavenPlugin, config_fixture: dict[str, str], monkeypatch: MonkeyPatch, tmp_path: Path
) -> CollectMavenPlugin:
    """
    Override and parametrize plugin_fixture to return CollectMavenPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])

    plugin_fixture.context.collect_root_dir = tmp_path
    plugin_fixture.config = dict(maven_command="mvn", maven_opts=["-D1", "-D2"])
    plugin_fixture.context.repositories[PurlType.MAVEN] = [
        Repository.parse_obj(dict(url="https://somewhere.com", description=""))
    ]

    return plugin_fixture


@pytest.mark.parametrize(
    argnames="completed_process_fixture", argvalues=[dict(returncode=0), dict(returncode=1)], indirect=True
)
def test_collect_maven(
    # pylint: disable=too-many-arguments
    plugin_fixture: CollectMavenPlugin,
    component: Component,
    completed_process_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
    run_command_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method
    """
    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=os, name="rename", value=lambda old, new: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    if completed_process_fixture.returncode == 0:
        assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    else:
        assert collect_result.is_fail()


def test_get_version(plugin_fixture: CollectMavenPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0


@pytest.mark.parametrize(argnames="context_fixture", argvalues=[dict(strict_repos=False)], indirect=True)
def test_maven_no_strict(context_fixture: HopprContext, monkeypatch: MonkeyPatch):
    """
    Test collect method: --no-strict flag
    """
    monkeypatch.setattr(target=context_fixture, name="strict_repos", value=False)
    monkeypatch.setattr(target=Path, name="is_file", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="read_text", value=lambda self, encoding: SETTINGS_XML)

    plugin = CollectMavenPlugin(context=context_fixture, config=None)
    assert plugin.system_repositories == [
        "https://repo.maven.apache.org/maven2",
        "https://somewhere.com",
        "https://somewhere.else.com",
    ]
