"""
Test module for CollectDockerPlugin class
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

from pathlib import Path
from subprocess import CompletedProcess

import pytest

from pytest import FixtureRequest, MonkeyPatch
from pytest_mock import MockerFixture

import hoppr.plugin_utils

from hoppr.core_plugins.collect_docker_plugin import CollectDockerPlugin
from hoppr.models.credentials import CredentialRequiredService, Credentials
from hoppr.models.manifest import Component
from hoppr.result import Result


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectDockerPlugin)])
def plugin_fixture(plugin_fixture: CollectDockerPlugin) -> CollectDockerPlugin:
    """
    Override and parametrize plugin_fixture to return CollectDockerPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest) -> Component:
    """
    Test Component fixture
    """
    param_dict = dict(getattr(request, "param", {}))

    param_dict["name"] = param_dict.get("name", "test-component")
    param_dict["purl"] = param_dict.get("purl", "pkg:docker/something/else@1.2.3")
    param_dict["type"] = param_dict.get("type", "file")

    return Component(**param_dict)


def _mock_get_repos(comp: Component) -> list[str]:  # pylint: disable=unused-argument
    """
    Mock _get_repos method
    """

    return ["http://somewhere.com", "https://somewhere.com"]


@pytest.mark.parametrize(
    argnames=["completed_process_fixture", "component", "source_image", "directory_string"],
    argvalues=[
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@1.2.3"},
            "docker://somewhere.com/something/else:1.2.3",
            "somewhere.com/something/else_1.2.3",
            id="semantic",
        ),
        pytest.param(
            {"returncode": 0},
            {
                "purl": "pkg:docker/something/else@sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00"  # pylint: disable=line-too-long
            },
            "docker://somewhere.com/something/else@sha256:ca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",  # pylint: disable=line-too-long
            "somewhere.com/something/else_sha256%3Aca5534a51dd04bbcebe9b23ba05f389466cf0c190f1f8f182d7eea92a9671d00",
            id="sha256",
        ),
        pytest.param(
            {"returncode": 0},
            {"purl": "pkg:docker/something/else@40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1"},
            "docker://somewhere.com/something/else@sha256:40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",  # pylint: disable=line-too-long
            "somewhere.com/something/else_sha256%3A40d8a1af82bcc8472cf251c7c47e7b217379801704f99c6c6dac3a30e036f4d1",
            id="hash",
        ),
    ],
    indirect=["completed_process_fixture", "component"],
)
def test_collect_docker_success(  # pylint: disable=too-many-arguments
    plugin_fixture: CollectDockerPlugin,
    component: Component,
    source_image: str,
    directory_string: str,
    completed_process_fixture: CompletedProcess,
    monkeypatch: MonkeyPatch,
    mocker: MockerFixture,
):
    """
    Test a successful run of the Docker Collector
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)

    mock_run_command = mocker.patch.object(
        target=plugin_fixture, attribute="run_command", return_value=completed_process_fixture
    )

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"

    mock_run_command.assert_called_once_with(
        [
            "skopeo",
            "copy",
            "--src-tls-verify=false",
            source_image,
            (f"docker-archive:{plugin_fixture.context.collect_root_dir}/" f"docker/http%3A%2F%2F{directory_string}"),
        ],
        [],
    )


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_docker_fail(
    plugin_fixture: CollectDockerPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredentialRequiredService,
):
    """
    Test a failing run of the Docker Collector
    """

    monkeypatch.setattr(target=Credentials, name="find", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(target=Path, name="exists", value=lambda path: True)
    monkeypatch.setattr(target=Path, name="unlink", value=lambda path: None)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Skopeo failed to copy docker image"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[{"skopeo_command": "skopeo"}], indirect=True)
def test_collect_docker_command_not_found(
    plugin_fixture: CollectDockerPlugin,
    config_fixture: dict[str, str],
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
):
    """
    Test if the required command is not found
    """
    # pylint: disable=duplicate-code
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=_mock_get_repos)
    monkeypatch.setattr(
        target=hoppr.plugin_utils,
        name="check_for_missing_commands",
        value=lambda message: Result.fail("[mock] command not found"),
    )

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"
