"""
Test module for BaseCollectorPlugin, BatchCollectorPlugin, and SerialCollectorPlugin classes
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import json

from pathlib import Path
from typing import Any, Literal

import pytest

from packageurl import PackageURL
from pytest import FixtureRequest, MonkeyPatch

from hoppr.base_plugins.collector import BatchCollectorPlugin, SerialCollectorPlugin
from hoppr.base_plugins.hoppr import hoppr_rerunner
from hoppr.constants import BomProps
from hoppr.models import HopprContext
from hoppr.models.credentials import CredentialRequiredService
from hoppr.models.manifest import Component, Property
from hoppr.result import Result, ResultStatus


class MyBatchCollectorPlugin(BatchCollectorPlugin):
    """
    BatchCollectorPlugin subclass with default behavior
    """

    supported_purl_types = ["good1", "good2"]

    def __init__(self, context: HopprContext, config: Any):
        super().__init__(context=context, config=config)
        self.collect_count = 0  # for counting the number of times collect is called

    def get_version(self):
        return "1.70.1"

    @hoppr_rerunner
    def collect(self, comp: Any) -> Result:
        self.collect_count += 1
        self.set_collection_params(comp, "the_batch_repo", Path(self.context.collect_root_dir / "the_batch_directory"))
        return Result.success(return_obj=comp)


class MyCollectorPlugin(SerialCollectorPlugin):
    """
    SerialCollectorPlugin subclass with default behavior
    """

    supported_purl_types = ["good1", "good2"]

    def __init__(self, context: HopprContext, config: Any):
        super().__init__(context=context, config=config)
        self.collect_count = 0  # for counting the number of times collect is called

    def get_version(self):
        return "1.70.1"

    @hoppr_rerunner
    def collect(self, comp: Any, repo_url: str, creds: CredentialRequiredService | None = None) -> Result:
        self.collect_count += 1
        self.set_collection_params(comp, "the_seq_repo", Path(self.context.collect_root_dir / "the_seq_directory"))
        return Result.success(return_obj=comp)


class MyCollectorPluginRetry(SerialCollectorPlugin):
    """
    SerialCollectorPlugin subclass that returns a retry result
    """

    supported_purl_types = ["good1", "good2"]
    system_repositories = ["alpha", "omega"]

    def __init__(self, context: HopprContext, config: Any):
        super().__init__(context=context, config=config)
        self.collect_count = 0  # for counting the number of times collect is called

    def get_version(self):
        return "1.70.1"

    @hoppr_rerunner
    def collect(self, comp: Any, repo_url: str, creds: CredentialRequiredService | None = None) -> Result:
        self.collect_count += 1
        return Result.retry("TestCollectorPlugin method collect failed, but might succeed on a subsequent attempt.")


@pytest.fixture(name="component")
def component_fixture(request: FixtureRequest) -> Component:
    """
    Test Component fixture
    """
    purl_string = getattr(request, "param", "pkg:good1/something/else@1.2.3")
    purl = PackageURL.from_string(purl_string)
    return Component.parse_obj(dict(name=purl.name, version=purl.version, purl=purl_string, type="file"))


@pytest.fixture(scope="function")
def batch_plugin_fixture(context_fixture: HopprContext, config_fixture: dict[str, str]) -> MyBatchCollectorPlugin:
    """
    Fixture to return MyCollectorPluginRetry
    """
    return MyBatchCollectorPlugin(context=context_fixture, config=config_fixture)


@pytest.fixture(scope="function", params=[dict(plugin_class=MyCollectorPlugin)])
def plugin_fixture(plugin_fixture: MyCollectorPlugin, config_fixture: dict[str, str]) -> MyCollectorPlugin:
    """
    Override and parametrize plugin_fixture to return MyCollectorPlugin
    """
    return plugin_fixture


@pytest.fixture(scope="function")
def retry_plugin_fixture(context_fixture: HopprContext, config_fixture: dict[str, str]) -> MyCollectorPluginRetry:
    """
    Fixture to return MyCollectorPluginRetry
    """
    return MyCollectorPluginRetry(context=context_fixture, config=config_fixture)


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_collector_base(plugin_fixture: MyCollectorPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test BaseCollectorPlugin base functionality
    """
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])
    assert plugin_fixture.config == dict(config="CONFIG")

    collect_result = plugin_fixture.process_component(comp=component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    assert plugin_fixture.collect_count == 1

    test_dir = plugin_fixture.directory_for("pypi", "https://www.pypi.org/")
    assert test_dir == plugin_fixture.context.collect_root_dir / "pypi" / "https%3A%2F%2Fwww.pypi.org"

    test_dir = plugin_fixture.directory_for("pypi", "https://www.pypi.org/", subdir="somewhere/deeper")
    assert test_dir == (
        plugin_fixture.context.collect_root_dir / "pypi" / "https%3A%2F%2Fwww.pypi.org" / "somewhere" / "deeper"
    )


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_collector_base_retry_fail(
    retry_plugin_fixture: MyCollectorPluginRetry, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test BaseCollectorPlugin.process_component fails after 3 retries
    """
    monkeypatch.setattr(target=retry_plugin_fixture, name="_get_repos", value=lambda comp: ["https://somewhere.com"])
    assert retry_plugin_fixture.config == dict(config="CONFIG")

    collect_result = retry_plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert retry_plugin_fixture.collect_count == 3


@pytest.mark.parametrize(
    argnames=["config_fixture", "component", "strict_repos", "expected_repos"],
    argvalues=[
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3", True, ["alpha", "beta", "gamma"]),
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3", False, ["alpha", "beta", "gamma", "omega"]),
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3?repository_url=beta", True, ["beta"]),
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3?repository_url=beta", False, ["beta"]),
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3?repository_url=delta", True, []),
        (dict(config="CONFIG"), "pkg:good1/something/else@1.2.3?repository_url=delta", False, ["delta"]),
    ],
    indirect=["config_fixture", "component"],
)
def test__get_repos(
    retry_plugin_fixture: MyCollectorPluginRetry, component: Component, strict_repos: bool, expected_repos: list[str]
):
    """
    Test BaseCollectorPlugin._get_repos method
    """
    component.properties = []
    component.properties.append(
        Property.parse_obj(
            dict(
                name=BomProps.COMPONENT_SEARCH_SEQUENCE,
                value=f'{{"version": "v1", "repositories": {json.dumps(expected_repos)}}}',
            )
        )
    )

    retry_plugin_fixture.context.strict_repos = strict_repos
    assert retry_plugin_fixture._get_repos(component) == expected_repos


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_collector_base_no_purl(plugin_fixture: MyCollectorPlugin, component: Component):
    """
    Test SerialCollectorPlugin.process_component method with component purl of None
    """
    component.purl = None

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "No purl supplied for component"


@pytest.mark.parametrize(
    argnames=["config_fixture", "component", "strict_repos", "search_repos", "expected_message"],
    argvalues=[
        (
            dict(config="CONFIG"),
            "pkg:good1/something/else@1.2.3?repository_url=beta",
            True,
            [],
            "No repositories specified in manifest for purl type good1. "
            "Add repositories or use --no-strict command line option",
        ),
        (
            dict(config="CONFIG"),
            "pkg:good1/something/else@1.2.3?repository_url=beta",
            False,
            [],
            "No repositories specified for purl type good1, and no repository_url specified in purl",
        ),
        (
            dict(config="CONFIG"),
            "pkg:good1/something/else@1.2.3?repository_url=beta",
            True,
            ["alpha", "beta", "gamma"],
            "Specified repository_url (beta) is not in manifest repository list.  "
            "Add it to the manifest, or use --no-strict command line option",
        ),
        (
            dict(config="CONFIG"),
            "pkg:good1/something/else@1.2.3?repository_url=beta",
            False,
            ["alpha", "beta", "gamma"],
            "No repositories specified for purl type good1, and no repository_url specified in purl",
        ),
        (
            dict(config="CONFIG"),
            "pkg:good1/something/else@1.2.3",
            True,
            ["alpha", "beta", "gamma"],
            "Reason for empty repo list undetermined",
        ),
    ],
    indirect=["config_fixture", "component"],
)
def test_collector_base_no_repos(  # pylint: disable=too-many-arguments
    plugin_fixture: MyCollectorPlugin,
    component: Component,
    strict_repos: bool,
    search_repos: list[str],
    expected_message: str,
    monkeypatch: MonkeyPatch,
):
    """
    Test SerialCollectorPlugin.process_component method with no repos
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=plugin_fixture, name="_get_repos", value=lambda component: [])
        patch.setattr(
            target=component,
            name="properties",
            value=[
                Property.parse_obj(
                    dict(
                        name=BomProps.COMPONENT_SEARCH_SEQUENCE,
                        value=f'{{"version": "v1", "repositories": {json.dumps(search_repos)}}}',
                    )
                )
            ],
        )

        plugin_fixture.context.strict_repos = strict_repos
        collect_result = plugin_fixture.process_component(component)
        assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
        assert collect_result.message == expected_message


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_post_stage_process(plugin_fixture: MyCollectorPlugin, monkeypatch: MonkeyPatch):
    """
    Test SerialCollectorPlugin.post_stage_process method
    """
    monkeypatch.setattr(target=Path, name="is_dir", value=lambda self: True)
    monkeypatch.setattr(target=Path, name="exists", value=lambda self: True)

    for purl_type in plugin_fixture.supported_purl_types:
        Path(plugin_fixture.context.collect_root_dir, purl_type).mkdir(parents=True, exist_ok=True)

    collect_result = plugin_fixture.post_stage_process()
    assert collect_result.is_success()


@pytest.mark.parametrize(
    argnames=["purl_string", "repo_url", "expected_result"],
    argvalues=[
        ("pkg:generic/location?repository_url=http://my.repo:80", "http://my.repo", "SUCCESS"),
        ("pkg:generic/location", "https://my.repo", "SUCCESS"),
        ("pkg:generic/location?repository_url=my.repo:443", "https://my.repo/", "SUCCESS"),
        ("pkg:generic/location?repository_url=my.repo", "http://my.repo/with/more/stuff", "SUCCESS"),
        ("pkg:generic/location?repository_url=http://my.repo", "https://your.repo", "FAIL"),
        ("pkg:generic/location?repository_url=my.repo:22", "ssh://my.repo", "SUCCESS"),
    ],
)
def test_check_purl_specified_url(purl_string: str, repo_url: str, expected_result: Literal["SUCCESS", "FAIL"]):
    """
    Test BaseCollectorPlugin.check_purl_specified_url method
    """
    purl: PackageURL = PackageURL.from_string(purl_string)  # pyright: ignore[reportGeneralTypeIssues]
    result = SerialCollectorPlugin.check_purl_specified_url(purl, repo_url)
    result_check = {"SUCCESS": result.is_success, "FAIL": result.is_fail}[expected_result]
    assert result_check(), f"Expected {expected_result} result, got {result}"


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_set_collect_params(plugin_fixture: MyCollectorPlugin, component: Component):
    """
    Test BaseCollectorPlugin.set_collection_params method
    """
    assert component.properties is None

    plugin_fixture.set_collection_params(component, "test_repo", f"{plugin_fixture.context.collect_root_dir}/test_dir")

    assert component.properties is not None
    assert len(component.properties) == 4

    for prop in component.properties:  # pyright: ignore[reportGeneralTypeIssues]
        if prop.name == BomProps.COLLECTION_REPOSITORY:
            assert prop.value == "test_repo"
        if prop.name == BomProps.COLLECTION_DIRECTORY:
            assert prop.value == "test_dir"

    plugin_fixture.set_collection_params(
        component, "another_repo", f"{plugin_fixture.context.collect_root_dir}/test_dir"
    )

    assert len(component.properties) == 4

    for prop in component.properties:  # pyright: ignore[reportGeneralTypeIssues]
        if prop.name == BomProps.COLLECTION_REPOSITORY:
            assert prop.value == "another_repo"
        if prop.name == BomProps.COLLECTION_DIRECTORY:
            assert prop.value == "test_dir"


@pytest.mark.parametrize(
    argnames=["input_result", "expected_result_status", "expected_result_message"],
    argvalues=[
        (
            Result.success(),
            ResultStatus.FAIL,
            (
                "Collector class MyCollectorPlugin process_component method returned a successful result "
                "without updating the BOM component."
            ),
        ),
        (
            Result.success(return_obj="Not a Component"),
            ResultStatus.FAIL,
            (
                "Collector class MyCollectorPlugin process_component method returned a successful result "
                "without updating the BOM component."
            ),
        ),
        (Result.fail("Some Message"), ResultStatus.FAIL, "Some Message"),
    ],
)
@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_check_collect_params_bad_return(
    plugin_fixture: MyCollectorPlugin,
    input_result: Result,
    expected_result_status: ResultStatus,
    expected_result_message: str,
):
    """
    Test BaseCollectorPlutin._check_collection_params method
    """

    test_result = plugin_fixture._check_collection_params(input_result)
    assert test_result.status == expected_result_status
    assert test_result.message == expected_result_message


@pytest.mark.parametrize(argnames="config_fixture", argvalues=[dict(config="CONFIG")], indirect=True)
def test_check_collect_params_some_missing(plugin_fixture: MyCollectorPlugin, component: Component):
    """
    Test BaseCollectorPlugin._check_collection_params method with required property missing
    """
    component.properties = []
    component.properties.append(Property(name=BomProps.COLLECTION_PLUGIN, value="DummyPlugin"))
    component.properties.append(Property(name=BomProps.COLLECTION_TIMETAG, value="Now"))

    assert len(component.properties) == 2

    input_result = Result.success(return_obj=component)
    test_result = plugin_fixture._check_collection_params(input_result)
    assert test_result.is_fail()
    assert test_result.message == (
        "Collector class MyCollectorPlugin process_component method returned a successful result "
        "without updating the following component properties: hoppr:collection:repository, "
        "hoppr:collection:directory"
    )
