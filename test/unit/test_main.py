"""
Test module for main module
"""

# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import test

from pathlib import Path

import pytest

from click.exceptions import Exit
from pytest import MonkeyPatch

import hoppr.main

from hoppr.main import HopprProcessor  # type: ignore[attr-defined]
from hoppr.result import Result


class MockProcessor:  # pylint: disable=too-few-public-methods
    """
    Mock HopprProcessor
    """

    metadata_files: list[str] = []

    def __init__(self, *args, **kwargs):
        super().__init__()

    def run(self, *args, **kwargs) -> Result:
        """
        Mock run method
        """
        return Result.fail()


@pytest.mark.parametrize(argnames="expected", argvalues=[Result.fail, Result.success])
def test_bundle(monkeypatch: MonkeyPatch, expected: Result):
    """
    Test bundle method
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: expected)

        if expected is Result.fail():
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                hoppr.main.bundle(
                    Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
                    Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
                    Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
                    Path("mylog.txt"),
                )

            assert pytest_wrapped_e.type == SystemExit
            assert pytest_wrapped_e.value.code == 1


def test_bundle_no_key(monkeypatch: MonkeyPatch):
    """
    Test bundle method without a key
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: Result.success())

        with pytest.raises(Exit) as pytest_wrapped_e:
            hoppr.main.bundle(
                Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
                Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
                Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
                Path("mylog.txt"),
                create_attestations=True,
            )

        assert pytest_wrapped_e.type == Exit


@pytest.mark.parametrize(argnames="func_key_prompt", argvalues=[True, False])
def test_bundle_attest_prompt(monkeypatch: MonkeyPatch, func_key_prompt: bool):
    """
    Test bundle method with and without prompt
    """
    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=hoppr.main, name="prompt", value=lambda *args, **kwargs: "5678")

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                Path(test.__file__).parent / "resources" / "manifest" / "manifest.yml",
                Path(test.__file__).parent / "resources" / "credential" / "cred-test.yml",
                Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
                log_file=Path("mylog.txt"),
                create_attestations=True,
                functionary_key_path=Path("mylog.txt"),
                functionary_key_prompt=func_key_prompt,
            )

        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1


@pytest.mark.parametrize(argnames="will_generate_prompt", argvalues=[True, False])
def test_generate_layout_prompt(monkeypatch: MonkeyPatch, will_generate_prompt: bool):
    """
    Test generate_layout fuction with and without prompt
    """

    product_owner_path = Path("product_owner_key")
    functionary_path = Path("functionary_key")

    with monkeypatch.context() as patch:
        patch.setattr(target=hoppr.main, name="HopprProcessor", value=MockProcessor)
        patch.setattr(target=HopprProcessor, name="run", value=lambda *args, **kwargs: Result.success())
        patch.setattr(target=hoppr.main, name="prompt", value=lambda *args, **kwargs: "5678")
        patch.setattr(target=hoppr.main, name="generate_in_toto_layout", value=lambda *args, **kwargs: None)

        hoppr.main.generate_layout(
            Path(test.__file__).parent / "resources" / "transfer" / "transfer-test.yml",
            product_owner_path,
            functionary_path,
            will_generate_prompt,
            "1234",
        )


def test_validate(monkeypatch: MonkeyPatch):
    """
    Test validate method
    """
    # pylint: disable=duplicate-code

    with monkeypatch.context() as patch:
        patch.setenv(name="SOME_USERNAME", value="pipeline-bot")
        patch.setenv(name="SOME_TOKEN", value="SoMe_ToKeN1234!@#$")
        patch.setenv(name="DOCKER_PW", value="DoCKeR_PW1234!@#$")
        patch.setenv(name="DOCKER_ADMIN_PW", value="AdminDockerPW")
        patch.setenv(name="GITLAB_PW", value="GiTLaB_PW1234!@#$")

        hoppr.main.validate(
            [Path("test", "resources", "manifest", "unit", "manifest.yml").resolve()],
            Path("test", "resources", "credential", "cred-test.yml").resolve(),
            Path("test", "resources", "transfer", "transfer-test.yml").resolve(),
        )
