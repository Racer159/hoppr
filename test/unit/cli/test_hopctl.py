"""
Test module for hopctl.py
"""
import re
import sys

import pytest

from pytest import CaptureFixture, MonkeyPatch

from hoppr.cli.hopctl import version as hopctl_version


@pytest.mark.parametrize(
    argnames=["is_tty", "expected_regex"],
    argvalues=[
        (True, ".+Hoppr Framework Version.*:.*Python Version.*:.*"),
        (False, "Hoppr Framework Version.*:.*Python Version.*:.*"),
    ],
    ids=["tty", "not_tty"],
)
def test_version(is_tty: bool, expected_regex: str, capsys: CaptureFixture, monkeypatch: MonkeyPatch):
    """
    Test `hopctl version` output
    """
    monkeypatch.setattr(target=sys.stdout, name="isatty", value=lambda: is_tty)

    hopctl_version()
    captured = capsys.readouterr()

    matched = re.match(
        pattern=f"^{expected_regex}$",
        string=captured.out,
        flags=re.DOTALL,
    )

    assert matched is not None
