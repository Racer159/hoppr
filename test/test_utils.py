import json
import os
import shutil
import sys

from pathlib import Path
from unittest import TestCase, mock

import pytest
import yaml

from ruamel.yaml import YAML
from ruamel.yaml.parser import ParserError as YAMLParserError

import hoppr.net
import hoppr.plugin_utils
import hoppr.utils

from hoppr.base_plugins.hoppr import HopprPlugin
from hoppr.exceptions import HopprLoadDataError, HopprPluginError

test_input_data = {
    "alpha": [1, 2, 3],
    "beta": ["dog", "cat"],
    "gamma": {"x": 42, "y": "why not", "z": ["mixed", 7, "array"]},
}


class MockedResponse:
    def __init__(self, status_code, content="test content", resp_url="redirect_url"):
        self.status_code = status_code
        self.content = content
        self.url = resp_url
        self.text = "text"

    def raise_for_status(self):
        pass

    def iter_content(*args, **kwargs):
        return ["one".encode("utf-8"), "two".encode("utf-8"), "three".encode("utf-8")]

    def close(val):
        pass


class UnitTestHelloPlugin(HopprPlugin):
    def get_version(self) -> str:
        return "3.4.5"

    def say_hello(self, name):
        print(f"Hiya {name}")


class_source = """
class UnitTestHelloPlugin(HopprPlugin):

    def get_version(self) -> str:
        return "3.4.5"

    def say_hello(self, name):
        print(f"Hiya {name}")
"""

multi_class_source = """
class UnitTestHelloPlugin(HopprPlugin):

    def get_version(self) -> str:
        return "3.4.5"

    def say_hello(self, name):
        print(f"Hiya {name}")

class Something_Else():
    x=42

"""


class TestUtils(TestCase):
    def test_dedup_list(self):
        list_in = ["one", "two", "three"]
        deduped = hoppr.utils.dedup_list(list_in)
        assert list_in == deduped

    @mock.patch("inspect.getsource", return_value=class_source)
    @mock.patch("inspect.getmembers", return_value=[("class2", UnitTestHelloPlugin)])
    @mock.patch("importlib.import_module", return_value=sys.modules.get(__module__))
    def test_plugin_instance_success(self, mock_import, mock_getmembers, mock_getsource):
        obj = hoppr.utils.plugin_instance("UnitTestHelloPlugin", context=None, config=None)
        assert obj.get_version() == "3.4.5"

    @mock.patch("inspect.isclass", return_value=True)
    @mock.patch(
        "inspect.getsource",
        side_effect=[
            multi_class_source,
            class_source,
            multi_class_source,
            "This cannot be found",
            multi_class_source,
            class_source,
        ],
    )
    @mock.patch(
        "inspect.getmembers",
        return_value=(
            ("class1", UnitTestHelloPlugin),
            ("class2", UnitTestHelloPlugin),
            ("class3", UnitTestHelloPlugin),
        ),
    )
    @mock.patch("importlib.import_module", return_value=sys.modules.get(__module__))
    def test_plugin_instance_fail_multi_obj(self, mock_import, mock_getmembers, mock_get_source, mock_isclass):
        with pytest.raises(HopprPluginError):
            obj = hoppr.utils.plugin_instance("UnitTestHelloPlugin", context=None)

    @mock.patch("inspect.isclass", return_value=False)
    @mock.patch("inspect.getsource", side_effect=["No classes here"])
    @mock.patch("inspect.getmembers", return_value=[("class1", UnitTestHelloPlugin)])
    @mock.patch("importlib.import_module", return_value=UnitTestHelloPlugin(context=None))
    def test_plugin_instance_fail_no_good_class(self, mock_import, mock_getmembers, mock_get_source, mock_isclass):
        with pytest.raises(HopprPluginError):
            obj = hoppr.utils.plugin_instance("plugin_stub_name", context=None)

    @mock.patch("importlib.import_module", side_effect=ModuleNotFoundError("whatever"))
    def test_tryit_fail_modulenotfound(self, mock_import):
        with pytest.raises(ModuleNotFoundError):
            obj = hoppr.utils.plugin_instance("plugin_stub_name", context=None)

    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.object(Path, "open", new=mock.mock_open(read_data=json.dumps(test_input_data, indent=4)))
    def test_load_file_json_good(self, mock_isfile):
        content = hoppr.utils.load_file(Path())

        assert content == test_input_data

    @mock.patch.object(YAML, "load", side_effect=YAMLParserError())
    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.object(Path, "open", new=mock.mock_open(read_data=json.dumps(test_input_data, indent=4)))
    def test_load_file_json_good_yamlparseerror(self, mock_isfile, mock_safe_load):
        with pytest.raises(expected_exception=HopprLoadDataError):
            content = hoppr.utils.load_file(Path())

    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.object(Path, "open", new=mock.mock_open(read_data=yaml.dump(test_input_data, indent=4)))
    def test_load_file_yaml_good(self, mock_isfile):
        content = hoppr.utils.load_file(Path())

        assert content == test_input_data

    @mock.patch.object(Path, "is_file", return_value=False)
    @mock.patch("builtins.open", new=mock.mock_open(read_data=json.dumps(test_input_data, indent=4)))
    def test_load_file_missing(self, mock_isfile):

        with pytest.raises(HopprLoadDataError):
            content = hoppr.utils.load_file(Path())

    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.object(Path, "open", new=mock.mock_open(read_data="  \n "))
    def test_load_file_empty(self, mock_isfile):

        with pytest.raises(HopprLoadDataError):
            content = hoppr.utils.load_file(Path("test"))

    @mock.patch.object(Path, "is_file", return_value=True)
    def test_load_string_empty(self, mock_isfile):

        with pytest.raises(HopprLoadDataError):
            content = hoppr.utils.load_string("")

    @mock.patch.object(Path, "is_file", return_value=True)
    @mock.patch.object(Path, "open", new=mock.mock_open(read_data="This can't be parsed"))
    def test_load_file_unparseable(self, mock_isfile):

        with pytest.raises(HopprLoadDataError):
            content = hoppr.utils.load_file(Path())

    def test_obscure_passwords(self):
        command = ["run", "-pwd", "pa$$word", "multi word argument"]
        obscured = hoppr.utils.obscure_passwords(command, ["pa$$word"])

        assert obscured == 'run -pwd [masked] "multi word argument"'

    def test_delete_folder_empty_root(self):
        with pytest.raises(FileNotFoundError):
            hoppr.utils.remove_empty(Path("emptydirectory"))

    def test_delete_folder_unempty_root(self):
        path = Path(__file__).absolute().parent

        directory = path.joinpath("resources", "directories")
        directory.mkdir(parents=True, exist_ok=True)

        with directory.joinpath("test-for-empty-folder-util").open(mode="a"):
            pass

        deleted = hoppr.utils.remove_empty(directory)
        self.assertEqual(len(deleted), 0)

        shutil.rmtree(directory)

    def test_delete_folder_with_empty_folder(self):
        path = Path(__file__).absolute().parent

        directory = path.joinpath("resources", "directories")
        directory.mkdir(parents=True, exist_ok=True)

        with open(os.path.join(directory, "test-for-empty-folder-util"), mode="a"):
            pass

        directory.joinpath("REMOVE").mkdir()

        deleted = hoppr.utils.remove_empty(directory)
        expected = set([path.joinpath("resources"), directory, directory.joinpath("REMOVE")])
        self.assertEqual(len(deleted.difference(expected)), 0)

        shutil.rmtree(directory)
