{
  "[dockerfile][ignore][properties][shellscript]": {
    "editor.defaultFormatter": "foxundermoon.shell-format",
    "editor.tabSize": 2
  },

  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.tabSize": 2
  },

  "[markdown]": {
    "editor.defaultFormatter": "DavidAnson.vscode-markdownlint"
  },

  "[python]": {
    "editor.codeActionsOnSave": {
      "source.organizeImports": true
    },
    "editor.defaultFormatter": "ms-python.black-formatter",
    "editor.tabSize": 4
  },

  "[toml]": {
    "editor.formatOnSave": false,
    "editor.insertSpaces": true,
    "editor.tabSize": 2
  },

  "[yaml]": {
    "editor.tabSize": 2,
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },

  "autoDocstring.docstringFormat": "google",
  "autoDocstring.startOnNewLine": true,

  "editor.defaultFormatter": "esbenp.prettier-vscode",
  "editor.detectIndentation": false,
  "editor.formatOnSave": true,
  "editor.insertSpaces": true,

  "evenBetterToml.formatter.alignEntries": true,
  "evenBetterToml.formatter.arrayAutoCollapse": true,
  "evenBetterToml.formatter.arrayAutoExpand": true,
  "evenBetterToml.formatter.compactArrays": true,
  "evenBetterToml.formatter.crlf": false,
  "evenBetterToml.formatter.indentEntries": true,
  "evenBetterToml.formatter.indentString": "  ",
  "evenBetterToml.formatter.indentTables": false,
  "evenBetterToml.formatter.reorderKeys": true,
  "evenBetterToml.formatter.trailingNewline": true,
  "evenBetterToml.schema.associations": {
    "pyproject.toml": "https://json.schemastore.org/pyproject.json"
  },
  "evenBetterToml.schema.catalogs": [
    "https://www.schemastore.org/api/json/catalog.json"
  ],
  "evenBetterToml.schema.enabled": true,
  "evenBetterToml.taplo.bundled": true,
  "evenBetterToml.taplo.configFile.enabled": false,

  "git.alwaysSignOff": true,
  "git.autofetch": true,
  "git.autoStash": true,
  "git.enableCommitSigning": true,
  "git.pruneOnFetch": true,
  "git.terminalAuthentication": true,
  "git.terminalGitEditor": true,
  "git.useIntegratedAskPass": true,

  "gitlab.showPipelineUpdateNotifications": true,
  "gitlens.showWelcomeOnInstall": false,
  "gitlens.showWhatsNewAfterUpgrades": false,

  "git-graph.dialog.fetchRemote.prune": true,
  "git-graph.repository.commits.showSignatureStatus": true,
  "git-graph.repository.fetchAndPrune": true,
  "git-graph.repository.sign.commits": true,
  "git-graph.repository.sign.tags": true,

  "isort.args": [
    "--atomic",
    "--lines-between-types=1",
    "--line-length=120",
    "--profile=black",
    "--skip-gitignore"
  ],
  "isort.check": true,

  "json.schemaDownload.enable": true,
  "json.schemas": [
    {
      "fileMatch": ["credentials.yml", "cred-test.yml"],
      "schema": true,
      "url": "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-credentials-schema-v1.json"
    },

    {
      "fileMatch": ["*manifest*.yml"],
      "schema": true,
      "url": "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-manifest-schema-v1.json"
    },

    {
      "fileMatch": ["transfer.yml", "transfer-test.yml"],
      "schema": true,
      "url": "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-transfers-schema-v1.json"
    }
  ],

  "markdown-preview-enhanced.frontMatterRenderingOption": "code block",
  "markdown-preview-enhanced.codeBlockTheme": "default.css",
  "markdown-preview-enhanced.previewTheme": "none.css",

  "markdownlint.config": {
    "first-line-heading": false,
    "no-inline-html": false
  },

  "prettier.printWidth": 120,
  "prettier.singleQuote": true,

  "python.analysis.autoImportCompletions": true,
  "python.analysis.completeFunctionParens": true,
  "python.analysis.typeCheckingMode": "basic",
  "python.defaultInterpreterPath": "python3",
  "python.formatting.provider": "black",
  "python.languageServer": "Pylance",
  "python.linting.mypyEnabled": true,
  "python.linting.mypyArgs": [
    "--follow-imports=silent",
    "--ignore-missing-imports",
    "--show-column-numbers",
    "--no-pretty",
    "--namespace-packages",
    "--explicit-package-bases",
    "--install-types",
    "--non-interactive",
    "--no-warn-incomplete-stub",
    "--show-error-codes"
  ],
  "python.linting.pylintEnabled": true,
  "python.poetryPath": "poetry",
  "python.terminal.activateEnvironment": true,
  "python.terminal.activateEnvInCurrentTerminal": true,
  "python.testing.autoTestDiscoverOnSaveEnabled": true,
  "python.testing.pytestArgs": ["--strict-markers", "--no-cov", "test"],
  "python.testing.pytestEnabled": true,
  "python.testing.unittestEnabled": false,

  "shellformat.flag": "-bn -kp -ci",

  "yaml.customTags": ["!reference sequence"],
  "yaml.format.printWidth": 160,
  "yaml.schemas": {
    "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-manifest-schema-v1.yml": [
      "*manifest*.yml"
    ],
    "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-transfers-schema-v1.yml": [
      "transfer-test.yml",
      "transfer.yml"
    ],
    "https://gitlab.com/api/v4/projects/hoppr%2Fhoppr/packages/generic/schemas/v1/hoppr-credentials-schema-v1.yml": [
      "cred-test.yml",
      "credentials.yml"
    ]
  }
}
